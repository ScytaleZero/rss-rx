"use strict" 
/**
 * Provides a set of utility functions for working with feeds.
 * @module feedUtils
 */
const feedparser = require("feedparser")
const request = require("request")

module.exports = {
  /** The maximum number of feed article IDs that will be cached. */
	HASH_LIMIT: 100, 
	MIN_UPDATE: (60 * 30), //30 minutes
	MAX_UPDATE: (60 * 60 * 24), //24 hours
  /** 
   * Returns a (hopefully) unique identifier for an item.
   */
  getHash(item) {
    if (item.guid) { 
      return item.guid 
    } else {
      const itemDate = Date.parse(item.pubdate)
      return item.title + itemDate
    }
  },
  /** 
   * Stores a hash, trimming the array if it is past the limit. 
   */
  storeHash(item, itemHashes) {
    itemHashes.unshift(this.getHash(item))
    if (itemHashes.length > this.HASH_LIMIT) {
      itemHashes.pop()
    }
  },
  /** 
   * Returns a feed item that shows the requested message. 
  */
  getMessageItem(message, title) {
    const item = {
      title: "",
      description: "",
      pubdate: (new Date()).toISOString()
    }
    if (message instanceof Error) {
      item.title = title || message.message
      item.description = message.stack || message.message
    } else {
      item.title = title || message
      item.description = message
    }
    item.guid = `${item.title}-${Date.now()}`
    return item
  },
  /** 
   * Returns a promise resulting in the parsed RSS or Atom feed.
  */
  parseFeed(options) {
    return new Promise( (resolve, reject) => {
      const items = []
      const parser = new feedparser()

      parser.on('error', (err) => {
        reject(err)
      });
      parser.on('readable', () => {
        let item
        while (item = parser.read()) { 
					items.push(item) 
				}
      })
      request.get(options)
				.on('error', reject)
				.on('response', function(res) {
					const stream = this
					if (res.statusCode !== 200) {
						reject(`Bad status code: ${res.statusCode}`);
					}
					else {
						stream.pipe(parser);
					}
				})
        .on('end', () => resolve(items))
    })
  },
  /** 
   * Analyzes an array of articles to determine the average number of seconds between updates for the feed.
   * @param {Object[]} items - Array of articles to examine.
   * @returns {Integer} Average seconds between updates, clamped between 30 minutes and 24 hours.
   */
  averageUpdateSeconds(items) {
		if (!items) return this.MIN_UPDATE
    const average = items
      .map((item, index, items) => {
        if (index === 0) return
        //Return difference between this and the previous time in seconds
        return Math.abs((Date.parse(item.pubdate) / 1000) - (Date.parse(items[index-1].pubdate) / 1000))
      })
      .filter((item, index) => index !== 0)
      .reduce((p, c) => p + c, 0) / (items.length - 1)
    return this.checkUpdateSeconds(average)
	},
	checkUpdateSeconds(seconds) {
		if (Number(seconds) !== Number(seconds)) return this.MIN_UPDATE
		if (seconds < this.MIN_UPDATE) return this.MIN_UPDATE
		if (seconds > this.MAX_UPDATE) return this.MAX_UPDATE
		return Math.floor(seconds)
	}
}