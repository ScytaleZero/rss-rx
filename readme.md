<a name="module_rss-rx"></a>

## rss-rx
Parses an RSS/Atom feed and emits unique articles using Rx.

<a name="exp_module_rss-rx--module.exports"></a>

### module.exports(url, [intervalSeconds]) ⇒ <code>Observable</code> ⏏
Returns an observable that emits unique items from an RSS feed. See tests for examples.

**Kind**: Exported function  
**Returns**: <code>Observable</code> - This observable will only complete if intervalSeconds is not specified. Otherwise it will continue to monitor the feed for new articles.  

| Param | Type | Description |
| --- | --- | --- |
| url | <code>String</code> | The location of the RSS feed. This can also be an options object that will be recognized by Request. |
| [intervalSeconds] | <code>Integer</code> &#124; <code>Boolean</code> | Time between refreshes of the source. Omit to calculate the refresh time from the initial RSS pull. Set to false for a single pull. |

