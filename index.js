"use strict"
/** 
 * Parses an RSS/Atom feed and emits unique articles using Rx. 
 * @module rss-rx
 */

const Rx = require("rxjs/Rx")
const feedUtils = require("./feedUtils")

/**
 * Returns an observable that emits unique items from an RSS feed. See tests for examples.
 * @param {String} url - The location of the RSS feed. This can also be an options object that will
 * be recognized by Request.
 * @param {(Integer|Boolean)} [intervalSeconds] - Time between refreshes of the source. Omit to calculate 
 * the refresh time from the initial RSS pull with a minimum of 30m and max of 24h. Set to false for a single pull.
 * @returns {Observable} This observable will only complete if intervalSeconds is false. Otherwise it 
 * will continue to monitor the feed for new articles.
 */
module.exports = (url, intervalSeconds) => {
  if (!url) throw new Error("An RSS URL must be provided.")
  return Rx.Observable.create(observer => {
    /** Array of item GUIDs that have been seen lately */
    const itemHashes = []
    const singleShot = (intervalSeconds === false) 
    let intervalId
    //Initiate a timer that parses the XML feed
    const checkFeed = function checkFeed() {
      feedUtils.parseFeed(url)
        .then(items => {
          items
            .filter(item => itemHashes.indexOf(feedUtils.getHash(item)) === -1)
            .forEach(item => {
              feedUtils.storeHash(item, itemHashes)
              observer.next(item)
            })
          if (singleShot) observer.complete()
        })
        .catch(err => {
          if (singleShot) {
            //Send an error and end the stream
            observer.error(err)
          } else {
            //Send a soft error since this could be transient
            observer.next(feedUtils.getMessageItem(err))
          }
        })
    }
    checkFeed()
    if (singleShot) return
    if (typeof intervalSeconds === "number") {
      intervalId = setInterval(checkFeed, feedUtils.checkUpdateSeconds(intervalSeconds) * 1000)
    } else {
      feedUtils.parseFeed(url)
        .then(items => {
          //Average the time differences in the feed
          const updateSeconds = feedUtils.averageUpdateSeconds(items)
          intervalId = setInterval(checkFeed, updateSeconds * 1000)
        })
        .catch(err => {
          observer.next(feedUtils.getMessageItem(`Unable to set dynamic timeout, defaulting to minimum interval: ${err.message}`, "Dynamic timeout error"))
          intervalId = setInterval(checkFeed, feedUtils.MIN_UPDATE * 1000)
        })
    }
    //Unsubscribing should clear the interval
    return function unsubscribe() {
      clearInterval(intervalId)
    }
  })
}