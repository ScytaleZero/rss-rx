const tape = require("tape")
const nock = require("nock")
const rssRx = require("../index")
const feedUtils = require("../feedUtils")
const items = require("./content/items")

tape("Item hashes", t => {
  t.plan(2)
  t.equal(feedUtils.getHash(items.itemWithGuid), "http://www.whompcomic.com/comic/belly-fancier", 
    "should find explicit guid")
  t.equal(feedUtils.getHash(items.itemWithoutGuid), "Whomp! - Belly Fancier1471849200000", 
    "should calculate missing guid")
})
tape("Hash storage", t => {
  t.plan(1)
  const hashes = []
  for (let i = 0; i < 105; i++) {
    feedUtils.storeHash(items.itemWithGuid, hashes)
  }
  t.equal(hashes.length, 100, "should only go up to hash limit")
})
tape("Generated item with description and title", t => {
  t.plan(3)
  const messageText = feedUtils.getMessageItem("Message body", "Message title")
  const messageTextItem = {
    title: "Message title",
    description: "Message body"
  }
  t.ok(Date.parse(messageText.pubdate) - Date.now() < 5000, "should have the current date")
  messageTextItem.pubdate = messageText.pubdate
  t.ok(messageText.guid.match(/Message title-\d+/), "should have a unique GUID")
  messageTextItem.guid = messageText.guid
  t.deepEqual(messageText, messageTextItem, "should have correct structure")
})
tape("Generated item with description only", t => {
  t.plan(2)
  const messageText = feedUtils.getMessageItem("Message body")
  const messageTextItem = {
    title: "Message body",
    description: "Message body"
  }
  t.ok(Date.parse(messageText.pubdate) - Date.now() < 5000, "should have the current date")
  messageTextItem.pubdate = messageText.pubdate
  messageTextItem.guid = messageText.guid
  t.deepEqual(messageText, messageTextItem, "should have correct structure")
})
tape("Generated item from error", t => {
  t.plan(2)
  const errorTest = new Error("Message body")
  const messageText = feedUtils.getMessageItem(errorTest)
  const messageTextItem = {
    title: "Message body",
    description: errorTest.stack
  }
  t.ok(Date.parse(messageText.pubdate) - Date.now() < 5000, "should have the current date")
  messageTextItem.pubdate = messageText.pubdate
  messageTextItem.guid = messageText.guid
  t.deepEqual(messageText, messageTextItem, "should have correct structure")
})
tape("Feed retrieval and parse", t => {
  t.plan(1)
  nock("http://feeds.arstechnica.com")
    .get("/arstechnica/index")
    .replyWithFile(200, "test/content/ArsTechnica.1.xml")
  feedUtils.parseFeed("http://feeds.arstechnica.com/arstechnica/index")
		.then(items => t.equal(items.length, 20, "should return 20 items"))
		.catch(err => t.fail('should not error'))
})
tape("Average update calculation", t => {
	t.plan(4)
	//Check with invalid items arrays
	t.equal(feedUtils.averageUpdateSeconds(undefined), (60 * 30), 'should return 30m for undefined items array')
	t.equal(feedUtils.averageUpdateSeconds([]), (60 * 30), 'should return 30m for empty items array')
	//Check with mock sources
  nock("http://feeds.arstechnica.com")
    .get("/arstechnica/index")
    .replyWithFile(200, "test/content/ArsTechnica.1.xml")
  feedUtils.parseFeed("http://feeds.arstechnica.com/arstechnica/index")
    .then(items => t.equal(feedUtils.averageUpdateSeconds(items), 12554, "Ars should return 12,554 seconds"))
  nock("http://nzbs.org")
    .get("/rss?t=6020&dl=1&i=23513&r=56b6f51fe14657e999287a8b283b7408")
    .replyWithFile(200, "test/content/NZBs.xml")
  feedUtils.parseFeed("http://nzbs.org/rss?t=6020&dl=1&i=23513&r=56b6f51fe14657e999287a8b283b7408")
    .then(items => t.equal(feedUtils.averageUpdateSeconds(items), 1800, "NZBs should return the minimum of 30 minutes"))
})
tape("RSS URL", t => {
  t.plan(1)
  t.throws(() => { rssRx() }, "should throw if not provided")
})
tape("Ars single pull", t => {
  t.plan(1)
  nock("http://feeds.arstechnica.com")
    .get("/arstechnica/index")
    .replyWithFile(200, "test/content/ArsTechnica.1.xml")
  let itemCount = 0
  const sub = rssRx("http://feeds.arstechnica.com/arstechnica/index", false)
    .subscribe(item => {
      itemCount++
    }, err => {
      t.fail("should not error")
    }, () => {
      t.equal(itemCount, 20, "should complete after 20 items")
    })
})
tape("Empty feed single pull", t => {
  t.plan(1)
  nock("http://www.google.com")
    .get("/alerts/feeds/15340260687622224947/7828829920195666015")
		.replyWithFile(200, "test/content/EmptyGoogleAlert.xml")
	let itemCount = 0
  const sub = rssRx("http://www.google.com/alerts/feeds/15340260687622224947/7828829920195666015", false)
    .subscribe(item => {
      itemCount++
    }, err => {
      t.fail("should not error")
    }, () => {
      t.equal(itemCount, 0, "should complete after 0 items")
    })
})
tape("Empty feed dynamic pull", t => {
  t.plan(1)
  nock("http://www.google.com")
    .get("/alerts/feeds/15340260687622224947/7828829920195666015")
		.times(2)
		.replyWithFile(200, "test/content/EmptyGoogleAlert.xml")
	let itemCount = 0
  const sub = rssRx("http://www.google.com/alerts/feeds/15340260687622224947/7828829920195666015")
    .subscribe(item => {
			console.log(item)
      itemCount++
    }, err => {
      t.fail("should not error")
    }, () => {
      t.fail("should not complete")
		})
	setTimeout(() => {
		t.equal(itemCount, 0, "should return 0 items")
		sub.unsubscribe()
	}, 3 * 1000)
	
})
tape("RSS single errors", t => {
  t.plan(2)
  rssRx("http://feeds.arstechnicazzz.com/arstechnica/index", false)
    .subscribe(item => {
      t.fail("should not emit items")
    }, err => {
      t.ok(err, `should emit an error: ${err.message}`)
    }, () => {
      t.fail("should not complete")
    })
  rssRx("http://feeds.arstechnica.com/arstechnica/indexzzz", false)
    .subscribe(item => {
      t.fail("should not emit items")
    }, err => {
      t.ok(err, `should emit an error: ${err.message}`)
    }, () => {
      t.fail("should not complete")
    })
})
tape("RSS 403 error", t => {
  t.plan(2)
  let itemCount = 0
  nock("http://feeds.arstechnica.com")
    .get("/arstechnica/index/403")
    .times(5)
    .reply(403)
  const sub = rssRx("http://feeds.arstechnica.com/arstechnica/index/403")
    .subscribe(item => {
      t.ok((item.title === "Bad status code: 403") || (item.title === "Dynamic timeout error"), `should emit errors: ${item.title}`)
    }, err => {
      t.fail(err, `should not emit an error: ${err.message}`)
    }, () => {
      t.fail("should not complete")
    })
  setTimeout(() => {
    sub.unsubscribe()
  }, 2 * 1000)
})
tape("Ars errors and items", t => {
  t.plan(2)
  const errorMessage = "Error mock"
  nock("http://feeds.arstechnica.com")
    .get("/arstechnica/index")
    .replyWithError(errorMessage)
    .get("/arstechnica/index")
    .replyWithFile(200, "test/content/ArsTechnica.1.xml")
    .get("/arstechnica/index")
    .times(5)
    .replyWithFile(200, "test/content/ArsTechnica.2.xml")
  let itemCount = 0
  const sub = rssRx("http://feeds.arstechnica.com/arstechnica/index", 1)
    .subscribe(item => {
      if (itemCount === 0) t.equal(item.title, errorMessage, "should emit error item first")
      itemCount++
    }, err => {
      t.fail("should not emit an error")
    }, () => {
      t.fail("should not complete")
    })
  setTimeout(() => {
    t.equal(itemCount, 1, "should return only 1 item since update interval is too low")
    sub.unsubscribe()
  }, 3 * 1000)
})
