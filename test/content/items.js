
module.exports = {
  "itemWithGuid": {
    title: "Whomp! - Belly Fancier",
    description: "</a><img src=\"http://www.whompcomic.com/comics/1471833145-2016-8-22-Belly-Fancier.jpg\" /><p>Many believe that cats domesticated themselves by finding a plentiful source of rodentia amongst humans&#39; food storage. I, however, believe that they are attracted by the deliciously salty belly sweat of the more rotund of humans. The 18th century author Dr Samuel Johnson was most well-known for his constant gut-nursing of his beloved feline, Hodge.</p><br />",
    summary: "<a href=\"http://www.whompcomic.com/comic/belly-fancier\"><img src=\"http://www.whompcomic.com/comicsthumbs/1471833145-2016-8-22-Belly-Fancier.jpg\" /><br />New comic!</a><br />Today's News:<br />",
    date: "2016-08-22T07:00:00.000Z",
    pubdate: "2016-08-22T07:00:00.000Z",
    pubDate: "2016-08-22T07:00:00.000Z",
    link: "http://www.whompcomic.com/comic/belly-fancier",
    guid: "http://www.whompcomic.com/comic/belly-fancier",
    author: "tech@thehiveworks.com",
    comments: null,
    origlink: null,
    image: { },
    source: { },
    categories: [ ],
    enclosures: [ ]
  },
  "itemWithoutGuid": {
    title: "Whomp! - Belly Fancier",
    description: "</a><img src=\"http://www.whompcomic.com/comics/1471833145-2016-8-22-Belly-Fancier.jpg\" /><p>Many believe that cats domesticated themselves by finding a plentiful source of rodentia amongst humans&#39; food storage. I, however, believe that they are attracted by the deliciously salty belly sweat of the more rotund of humans. The 18th century author Dr Samuel Johnson was most well-known for his constant gut-nursing of his beloved feline, Hodge.</p><br />",
    summary: "<a href=\"http://www.whompcomic.com/comic/belly-fancier\"><img src=\"http://www.whompcomic.com/comicsthumbs/1471833145-2016-8-22-Belly-Fancier.jpg\" /><br />New comic!</a><br />Today's News:<br />",
    date: "2016-08-22T07:00:00.000Z",
    pubdate: "2016-08-22T07:00:00.000Z",
    pubDate: "2016-08-22T07:00:00.000Z",
    link: "http://www.whompcomic.com/comic/belly-fancier",
    author: "tech@thehiveworks.com",
    comments: null,
    origlink: null,
    image: { },
    source: { },
    categories: [ ],
    enclosures: [ ]
  }
}